@extends('admin/master')
@section('contentcp')
@if(!Auth::check())
  <script>window.location = "admin/login";</script>
@endif
<script src="sourceadmin/js/jquery-3.3.1.js"></script>
<script>
        $(function(){ // this will be called when the DOM is ready
          $('#priceedit').keyup(function() {
            var $form = $("#formedit");
            var $input = $form.find("#priceedit");
        
            // When user select text in the document, also abort.
            var selection = window.getSelection().toString();
                if ( selection !== '' ) {
                    return;
                }
                
                // When the arrow keys are pressed, abort.
                if ( $.inArray( event.keyCode, [38,40,37,39] ) !== -1 ) {
                    return;
                }
                
                
                var $this = $( this );
                
                // Get the value.
                var input = $this.val();
                
                var input = input.replace(/[\D\s\._\-]+/g, "");
                    input = input ? parseInt( input, 10 ) : 0;
        
                    $this.val( function() {
                        return ( input === 0 ) ? "0" : input.toLocaleString( "de-DE" );
                } );
          });
        });
        
        $(function(){ // this will be called when the DOM is ready
          $('#promotion_priceadd').keyup(function() {
            var $form = $("#formadd");
            var $input = $form.find("#formedit");
            var $input1 = $form.find("#promotion_priceadd");
        
            // When user select text in the document, also abort.
            var selection = window.getSelection().toString();
                if ( selection !== '' ) {
                    return;
                }
                
                // When the arrow keys are pressed, abort.
                if ( $.inArray( event.keyCode, [38,40,37,39] ) !== -1 ) {
                    return;
                }
                
                
                var $this = $( this );
                
                // Get the value.
                var input = $this.val();
                
                var input = input.replace(/[\D\s\._\-]+/g, "");
                    input = input ? parseInt( input, 10 ) : 0;
        
                    $this.val( function() {
                        return ( input === 0 ) ? "0" : input.toLocaleString( "de-DE" );
                } );
          });
        });
        </script>
        
    <!-- Begin page content -->
    <div class="container">
      <div class="page-header">
          @if(Session::has('thongbao'))
            <div class="alert alert-success">{{ Session::get('thongbao') }}</div>
          @endif

          @if(count($errors) > 0)
            @foreach($errors->all() as $er)
                <div class="alert alert-danger">{{ $er }}</div>
            @endforeach
          @endif
        <h2>Sửa chi tiết sản phẩm</h2>
      </div>

      <div class="row">
          <div class="col-md-4">
          <form action="{{ route('suasanpham', $product_get->id)}}" name="formedit" method="post" enctype="multipart/form-data">
            @csrf
                    <div class="form-group">
                        <input type="hidden" class="form-control" name="idedit" id="exampleInputEmail1" placeholder="Tên của sản phẩm" value="{{ $product_get->id }}">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Tên sản phẩm</label>
                        <input type="text" class="form-control" name="tensp" id="exampleInputEmail1" placeholder="Tên của sản phẩm" value="{{ $product_get->name }}" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Mô tả ngắn về sản phẩm</label>
                        <input type="text" class="form-control" id="exampleInputPassword1" name="motangan" placeholder="Mô tả ngắn" value="{{ $product_get->short_description }}" required>
                    </div>
                    <div class="form-group">
                    <label for="exampleInputPassword1">Loại sản phẩm hiện tại là: <span style="color: #eb008b;">{{ $product_type_get_one->name }}</span></label>
                        <select class="form-control" name="loaisp">
                            @foreach($product_type_get_all as $ptga)
                                <option value="{{ $ptga->id }}">{{ $ptga->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                            <label for="exampleInputPassword1">Danh mục con hiện tại là: <span style="color: #eb008b;">{{ $sub_category_get_one->name }}</span></label>
                            <select class="form-control" name="danhmuccon">
                                @foreach($sub_category_get_all as $scga)
                            <option value="{{ $scga->id }}">{{ $scga->name}}</option>
                                @endforeach
                            </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputFile">Tải lên hình ảnh cho sản phẩm</label>
                        <input type="file" name="fileupload" id="exampleInputFile"><br>
                        <input type="file" name="fileupload1" id="exampleInputFile"><br>
                        <input type="file" name="fileupload2" id="exampleInputFile"><br>
                        <input type="file" name="fileupload3" id="exampleInputFile"><br>
                        <p class="help-block">Tải lên ít nhất một hình ảnh cho sản phẩm.</p>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputFile">Tích vào ô để đặt sản phẩm là nổi bật</label>
                        <br>
                        <input type="checkbox" @if($product_get->top == 1) checked @endif name="noibat"> Nổi bật
                    </div>
                    <button type="submit" name="submit" class="btn btn-success">Cập nhật sản phẩm</button>
          </div>
          <div class="col-md-8">
                    <div class="form-group">
                            <label for="exampleInputEmail1">Giá gốc</label>
                            <input type="text" class="form-control" name="giagoc" id="priceedit" placeholder="Giá gốc" value="{{ $product_get->unit_price }}" required>
                    </div>
                    <div class="form-group">
                            <label for="exampleInputEmail1">Giá khuyễn mãi</label>
                            <input type="text" class="form-control" name="giakhuyenmai" id="promotion_priceadd" placeholder="Giá khuyến mãi" value="{{ $product_get->promotion_price }}" required>
                        </div>
                    <div class="form-group">
                        <label for="exampleInputFile">Hình ảnh sản phẩm hiện tại</label><br>
                    <img src="source/uploads/product/{{ $product_get->image }}" width="120" height="120" class="img-rounded" alt="">
                        <img src="source/uploads/product/{{ $product_get->image1 }}" width="120" height="120" class="img-rounded" alt="">
                        <img src="source/uploads/product/{{ $product_get->image2 }}" width="120" height="120" class="img-rounded" alt="">
                        <img src="source/uploads/product/{{ $product_get->image3 }}" width="120" height="120" class="img-rounded" alt="">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Mô tả chi tiết sản phẩm</label>
                        <textarea name="editor1" id="editor1" rows="10" cols="15" required>
                                {{ $product_get->long_description }}
                        </textarea>
                    </div>
                </form>
          </div>
      </div>
      <br><br>
    </div>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="sourceadmin/js/jquery-3.3.1.js"></script>
    <script src="sourceadmin/js/jquery.dataTables.min.js"></script>
    <script src="sourceadmin/js/dataTables.bootstrap.min.js"></script>
    <script src="sourceadmin/js/bootstrap.min.js"></script>
@endsection