@extends('pages/master')
@section('content')
<style>
    .navigation-wrap {
        height: 90px;
    }
</style>
<!-- MAIN CONTENT AREA -->
<div class="container-fluid">
        <div class="row">


            <div class="site-content shop-content-area col-sm-12 content-with-products description-area-before"
                role="main">


                <div class="single-breadcrumbs-wrapper">
                    <div class="container">
                        <a href="{{ url()->previous() }}"
                            class="basel-back-btn basel-tooltip"><span>Back</span></a>
                        <nav class="woocommerce-breadcrumb"><a href="{{ route('trangchu') }}">Trang chủ</a><a class="breadcrumb-last"
                                href="{{ route('chitiet', [$product->id, $product->id_type]) }}">{{ $product->name }}</a></nav>
                    </div>
                </div>

                <div class="container">
                    <div class="woocommerce-notices-wrapper"></div>
                </div>
                <div id="product-140094"
                    class="single-product-page single-product-content product-design-alt product-with-attachments post-140094 product type-product status-publish has-post-thumbnail product_cat-cham-soc-da product_cat-kem-chong-nang first instock shipping-taxable purchasable product-type-simple">

                    <div class="container">

                        <div class="row" data-sticky_parent>
                            <div class="product-image-summary col-sm-9" data-sticky_column>
                                <div class="row">
                                    <div class="col-sm-6 product-images">
                                        <div class="woocommerce-product-gallery woocommerce-product-gallery--with-images woocommerce-product-gallery--columns-4 images images row thumbs-position-bottom image-action-none"
                                            style="opacity: 0; transition: opacity .25s ease-in-out;">
                                            <div class="col-sm-12">
                                                <figure
                                                    class="woocommerce-product-gallery__wrapper owl-carousel">
                                                    <figure
                                                        data-thumb="uploads/product/{{ $product->image }}"
                                                        class="woocommerce-product-gallery__image"><a
                                                            href="uploads/product/{{ $product->image }}"><img
                                                                width="500" height="500"
                                                                src="uploads/product/{{ $product->image }}"
                                                                class="lazy lazy-hidden wp-post-image jetpack-lazy-image"
                                                                alt="" title="aloe 3" data-caption=""
                                                                data-src="uploads/product/{{ $product->image }}"
                                                                data-large_image="uploads/product/{{ $product->image }}"
                                                                data-large_image_width="500"
                                                                data-large_image_height="500"
                                                                data-lazy-type="image"
                                                                data-lazy-src="uploads/product/{{ $product->image }}"
                                                                data-lazy-srcset="uploads/product/{{ $product->image }}"
                                                                /></a>
                                                    </figure>
                                                    <figure
                                                        data-thumb="uploads/product/{{ $product->image1 }}"
                                                        class="woocommerce-product-gallery__image"><a
                                                            href="uploads/product/{{ $product->image1 }}"><img
                                                                width="600" height="599"
                                                                src="uploads/product/{{ $product->image1 }}"
                                                                class="lazy lazy-hidden jetpack-lazy-image"
                                                                alt="" title="aloe 1" data-caption=""
                                                                data-src="uploads/product/{{ $product->image1 }}"
                                                                data-large_image="uploads/product/{{ $product->image1 }}"
                                                                data-large_image_width="897"
                                                                data-large_image_height="895"
                                                                data-lazy-type="image"
                                                                data-lazy-src="uploads/product/{{ $product->image1 }}"
                                                                /></a>
                                                    </figure>
                                                    <figure
                                                        data-thumb="uploads/product/{{ $product->image2 }}"
                                                        class="woocommerce-product-gallery__image"><a
                                                            href="uploads/product/{{ $product->image2 }}"><img
                                                                width="600" height="600"
                                                                src="uploads/product/{{ $product->image2 }}"
                                                                class="lazy lazy-hidden jetpack-lazy-image"
                                                                alt="" title="aloe 2" data-caption=""
                                                                data-src="uploads/product/{{ $product->image2 }}"
                                                                data-large_image="uploads/product/{{ $product->image2 }}"
                                                                data-large_image_width="960"
                                                                data-large_image_height="960"
                                                                data-lazy-type="image"
                                                                data-lazy-src="uploads/product/{{ $product->image2 }}"
                                                                 /></a>
                                                    </figure>
                                                    <figure
                                                        data-thumb="uploads/product/{{ $product->image3 }}"
                                                        class="woocommerce-product-gallery__image"><a
                                                            href="uploads/product/{{ $product->image3 }}"><img
                                                                width="600" height="600"
                                                                src="uploads/product/{{ $product->image3 }}"
                                                                class="lazy lazy-hidden jetpack-lazy-image"
                                                                alt="" title="aloe 2" data-caption=""
                                                                data-src="uploads/product/{{ $product->image3 }}"
                                                                data-large_image="uploads/product/{{ $product->image3 }}"
                                                                data-large_image_width="960"
                                                                data-large_image_height="960"
                                                                data-lazy-type="image"
                                                                data-lazy-src="uploads/product/{{ $product->image3 }}"
                                                                 /></a>
                                                    </figure>
                                                </figure>
                                            </div>

                                            <div class="col-sm-12">
                                                <div class="thumbnails"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 summary entry-summary">
                                        <div class="summary-inner ">
                                            <div class="basel-scroll-content">


                                                <h1 itemprop="name" class="product_title entry-title">{{ $product->name }}</h1>
                                                <p class="price"><span class="price"><span class="woocommerce-Price-amount amount">
                                                    @if($product->promotion_price != 0)
                                                        <span style="text-decoration: line-through; color: #a52a2a;">
                                                            {{ number_format(($product->unit_price), 0, '.', ',') }}đ</span> - {{ number_format(($product->promotion_price), 0, '.', ',') }}đ
                                                        @else
                                                            {{ number_format(($product->unit_price), 0, '.', ',') }}đ
                                                        @endif
                                                        </span>
                                                    </span>


                                                <form class="cart"
                                                    action="atery-cooling-sun-cream-spf50-pa-200g"
                                                    method="post" enctype='multipart/form-data'>
                                                    <p>{{ $product->short_description }}</p>

                                                    <a href="{{ route('cartAdd', $product->id) }}" style="display: inline-block;
                                                            text-align: center;
                                                            font-size: 14px;
                                                            padding-top: 10px;
                                                            padding-bottom: 10px;
                                                            padding-left: 20px;
                                                            padding-right: 20px;
                                                            line-height: 18px;
                                                            text-transform: uppercase;
                                                            letter-spacing: .3px;
                                                            border-radius: 0;
                                                            border: 1px solid;">Thêm vào giỏ</a>
                                                    <a href="{{ route('buyNow', $product->id) }}"
                                                        style="    display: inline-block;
                                                        padding: 4px 0;
                                                        width: 150px;
                                                        font-size: 16px;
                                                        font-weight: 600;
                                                        color: #fff;
                                                        text-transform: uppercase;
                                                        text-align: center;
                                                        border: 1px solid #EF0A0A;
                                                        border-radius: 0px;
                                                        -webkit-border-radius: 0px;
                                                        -moz-border-radius: 0px;
                                                        background: #EF0A0A;
                                                        float: none;
                                                        margin-left: 0%;"><span class="buynow">MUA
                                                            NGAY</span></a>

                                                </form>


                                                <script type='text/javascript'>
                                                    (function ($) {
                                                        $('#suntory_add_to_cart').click(function () {
                                                            fbq('track', 'AddToCart', {
                                                                content_name: 'Kem Chống Nắng Skinfood Aloe Watery Cooling Sun Cream SPF50+ PA++++ (200g)',
                                                                content_category: 'Health & Beauty > Personal Care > Cosmetics',
                                                                content_ids: ['140094'],
                                                                content_type: 'product',
                                                                value: 540000,
                                                                currency: 'VND'
                                                            });
                                                        });
                                                    })(jQuery);</script>
                                                <div class="product_meta">

                                                </div>
                                                <div class="call-order">
                                                    <p style="color: #d00f0f;">Gọi đặt mua và kiểm tra hàng tại
                                                        cửa hàng:<strong><a href="tel:1900.xxx.xxx"> 1900.xxx.xxx
                                                            </a></strong></p>
                                                    <p>hoặc <strong><a href="tel:1900.xxx.xxx">1900.xxx.xxx</a></strong> </p>
                                                    <p>Thời gian hoạt động: 9h - 21h </p>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div><!-- .summary -->

                                <!-- .TAB -->
                                <div class="product-tabs-wrapper col-sm-12 hidden-xs">


                                    <div class="woocommerce-tabs wc-tabs-wrapper tabs-layout-tabs">
                                        <ul class="tabs wc-tabs">
                                            <li class="description_tab">
                                                <a href="{{ route('chitiet', [$product->id, $product->id_type]) }}">Mô tả sản phẩm</a>
                                            </li>
                                            <div class="clearfix"></div>
                                        </ul>
                                        <div class="basel-tab-wrapper">
                                            <a href="#tab-description"
                                                class="basel-accordion-title tab-title-description">Mô tả</a>
                                            <div class="woocommerce-Tabs-panel woocommerce-Tabs-panel--description panel entry-content wc-tab"
                                                id="tab-description">


                                                <h3 style="text-align: center;">{{ $product->name }}</h3> <br>
                                                {!! $product->long_description !!}

                                                <div id='jp-relatedposts' class='jp-relatedposts'>
                                                    <h3 class="jp-relatedposts-headline"><em>Có liên quan</em>
                                                    </h3>
                                                </div>
                                            </div>
                                        </div>

                                    </div>


                                </div>
                                <!-- .TAB -->
                            </div>
                            <!-- .SIDEBAR -->
                            <div id="secondary" class="col-lg-3 col-sm-3 hidden-xs" data-sticky_column>
                                <div id="woocommerce_recently_viewed_products-3" class="sidebar-widget woocommerce widget_recently_viewed_products"><h5 class="widget-title">Sản phẩm mới nhất</h5><ul class="product_list_widget">
                                    @foreach($product_new as $pn)
                                    <li>
    
    <a href="{{ route('chitiet', [$pn->id, $pn->id_type]) }}">
        <img width="300" height="300" src="uploads/product/{{ $pn->image }}" class="lazy lazy-hidden" alt="">       <span class="product-title">{{ $pn->name }}</span>
    </a>

    
    <span class="woocommerce-Price-amount amount">@if($pn->promotion_price != 0) {{ number_format(($pn->promotion_price), 0, ',', '.') }} @else {{ number_format(($pn->unit_price), 0, ',', '.') }} @endif<span class="woocommerce-Price-currencySymbol">₫</span></span>
    </li>
    @endforeach
                            </div>
                            <!-- END SIDEBAR -->


                        </div>
                    </div>


                    <div class="product-tabs-wrapper col-sm-12 mobile-tab visible-xs">
                        <section class="card  ">
                            <div class="header">Thông tin sản phẩm</div>
                            <div class="single-product-meta-wrapper"></div>
                        </section>
                        <section class="card">
                            <div class="header">Mô tả sản phẩm</div>
                            <div class="content single-product-content-wrapper" style="background: #fff">
                                <div class="single-product-content-inner">
                                </div>
                            </div>
                        </section>
                    </div>


                </div><!-- #product-140094 -->





            </div>
            <div class="clearfix"></div>

        </div> <!-- end row -->
    </div> <!-- end container -->
@endsection('content')