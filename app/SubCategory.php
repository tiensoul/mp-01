<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{
    protected $table = 'subcategory';

    public function type_product()
    {
        return $this->belongsTo('App\TypeProduct', 'id_type', 'id');
    }
}
