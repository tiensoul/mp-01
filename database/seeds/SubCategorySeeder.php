<?php

use Illuminate\Database\Seeder;

class SubCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('subcategory')->insert([
            ['name' => 'Eyes Makeup', 'id_sub' => '1'],
            ['name' => 'Face Makeup', 'id_sub' => '1'],
            ['name' => 'Lips Makeup', 'id_sub' => '1'],
            ['name' => 'Kem chống nắng', 'id_sub' => '2'],
            ['name' => 'Kem dưỡng da', 'id_sub' => '2'],
            ['name' => 'Nước hoa nam', 'id_sub' => '5'],
            ['name' => 'Nước hoa nữ', 'id_sub' => '5'],
            ['name' => 'Dầu gội', 'id_sub' => '4'],
            ['name' => 'Dầu xả', 'id_sub' => '4'],
            ['name' => 'Kem ủ tóc', 'id_sub' => '4'],
            ['name' => 'Dụng cụ cá nhân', 'id_sub' => '5'],
            ['name' => 'Dụng cụ trang điểm', 'id_sub' => '5']
        ]);
    }
}
